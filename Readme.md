# RickAndMorty (test app)
Реализовать мини-приложение Рика и Морти из дизайна по ссылке:
https://www.figma.com/file/JUuVDTfYCwDeuY1EsR76qH/Untitled?type=design&node-id=0%3A1&mode=design&t=BauCEhk5b3S1ckXO-1

Для получения данных использовать открытый API: https://rickandmortyapi.com/api

Экраны:
Стартовый экран (Launch Screen)
Экран списка персонажей - Необходимо реализовать, используя UIKit. Верстка кодом без сторонних библиотек
Детальный экран персонажа - Если есть опыт работы со SwiftUI, то реализовать через него, если нет - используем UIKit

Примечание: Цвета и картинки берем из макета Figma, шрифты можно использовать системные. Для работы с сетью используем URLSession. Launch Screen просто статичная картинка. На остальных экранах во время загрузки данных отображаем лоадер

Код необходимо предоставить в виде ссылка на репозиторий в github 


| LaunchScreen          | MainScreen           |
| --------------------- | -------------------- |
| ![Screenshot 1](ScreenshotsForReadme/LaunchScreen.png) | ![Screenshot 2](ScreenshotsForReadme/MainScreen.png) |

| CharacterScreen       |                      |
| --------------------- | -------------------- |
| ![Screenshot 1](ScreenshotsForReadme/CharacterScreen.png) | |
