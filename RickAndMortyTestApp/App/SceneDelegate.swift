
import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let window = UIWindow(windowScene: windowScene)
        
        let networkService = NetworkService(baseURL: "https://rickandmortyapi.com/api")
        let characterService = CharacterService(networkService: networkService)
        let locationService = LocationService(networkService: networkService)
        let episodeService = EpisodeService(networkService: networkService)
        
        let charactersViewModel = CharactersViewModel(characterService: characterService,
                                                      episodeService: episodeService,
                                                      locationService: locationService)
        
        window.rootViewController = UINavigationController(rootViewController: CharactersViewController(charactersViewModel: charactersViewModel))
        self.window = window
        self.window?.makeKeyAndVisible()
    }
}

