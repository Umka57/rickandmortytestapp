
import Foundation

// MARK: - Response
struct ResponseDataModel: Codable {
    let info: InfoModel?
    let results: [CharacterModelShort]?
}

// MARK: - Info
struct InfoModel: Codable {
    let count, pages: Int?
    let next: String?
    let prev: String?
}
