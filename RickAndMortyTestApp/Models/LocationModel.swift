
import Foundation

// MARK: - Location
struct LocationModel: Codable {
    let name, type, dimension, url: String?
}
