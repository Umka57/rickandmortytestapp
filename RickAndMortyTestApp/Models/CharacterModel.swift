
import Foundation

// MARK: - Model for List of Characters
struct CharacterModelShort: Codable {
    let id: Int?
    let name: String?
    let image: String?
}

// MARK: - Model with detail character info
struct CharacterModel: Codable {
    let id: Int?
    let name: String?
    let status: String?
    let species: String?
    let type: String?
    let gender: String?
    let origin: LocationModel?
    let image: String?
    let episode: [String]?
}
