
import Foundation

// MARK: - Episode
struct EpisodeModel: Codable, Hashable {
    let id: Int?
    let name, airDate, episode: String?
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case airDate = "air_date"
        case episode
    }
}
