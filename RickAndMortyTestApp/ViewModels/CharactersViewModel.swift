
import Foundation

final class CharactersViewModel {
    
    var onCharactersUpdated: (() -> Void)?
    var onErrorMessage: ((NetworkServiceError) -> Void)?
    var onLoadingStateUpdated: (() -> Void)?
    
    private(set) var isLoading: Bool? = true {
        didSet {
            onLoadingStateUpdated?()
        }
    }
    
    private let characterService: CharacterServiceProtocol
    private let episodeService: EpisodeServiceProtocol
    private let locationService: LocationServiceProtocol
    
    private(set) var characters: [CharacterModelShort] = [] {
        didSet {
            onCharactersUpdated?()
        }
    }
    
    private var page: Int = 1
    
    init(characterService: CharacterServiceProtocol,
         episodeService: EpisodeServiceProtocol,
         locationService: LocationServiceProtocol) {
        self.characterService = characterService
        self.episodeService = episodeService
        self.locationService = locationService
    }
    
    func loadCharacters() {
        characterService.getCharacters(page: self.page, completion: { [weak self] result in
            switch result {
            case .success(let characters):
                self?.isLoading = false
                self?.characters = characters
            case .failure(let error):
                self?.onErrorMessage?(error)
            }
        })
    }
    
    func getServices() -> (CharacterServiceProtocol, EpisodeServiceProtocol, LocationServiceProtocol){
        return (self.characterService, self.episodeService, self.locationService)
    }
}
