
import Foundation

final class CharacterViewModel: ObservableObject {
    
    private let characterService: CharacterServiceProtocol
    private let episodeService: EpisodeServiceProtocol
    private let locationService: LocationServiceProtocol
    
    private var characterId: Int
    
    @Published var character: CharacterModel?
    @Published var episodes: [EpisodeModel] = []
    @Published var location: LocationModel?
    
    init(characterId: Int,
         characterService: CharacterServiceProtocol,
         episodeService: EpisodeServiceProtocol,
         locationService: LocationServiceProtocol) {
        self.characterId = characterId
        self.characterService = characterService
        self.episodeService = episodeService
        self.locationService = locationService
    }
    
    func updateCharacterId(with id:Int) {
        self.characterId = id
    }
    
    func loadCharacterInfo() {
        
        characterService.getCharacterInfo(characterId: characterId, completion: { [weak self] result in
            switch result {
            case .success(let character):
                self?.character = character
            case .failure(let error):
                print(error.localizedDescription)
            }
        })
    }
    
    func loadEpisodes() {
        
        var errors: [NetworkServiceError] = []
        
        character?.episode?.forEach({ episode in
            if let episodeId = episode.components(separatedBy: "/").last {
                episodeService.getEpisodesInfo(episodeId: Int(episodeId) ?? 0, completion: { [weak self] result in
                    switch result {
                    case .success(let episode):
                        self?.episodes.append(episode)
                    case .failure(let error):
                        errors.append(error)
                    }
                })
            }
        })
    }
    
    func loadOrigin() {
        
        guard let originURL = character?.origin?.url else { return }
        if let originId = originURL.components(separatedBy: "/").last {
            locationService.getLocation(locationId: Int(originId) ?? 0, completion: { [weak self] result in
                switch result {
                case .success(let location):
                    self?.location = location
                case .failure(let error):
                    print(error.localizedDescription)
                }
            })
        }
    }
    
}
