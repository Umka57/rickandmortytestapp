
import UIKit
import SwiftUI

final class CharactersViewController: UIViewController {

    // MARK: Variables
    private let charactersViewModel: CharactersViewModel
    
    // MARK: UI Components
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(CharacterCollectionViewCell.self, forCellWithReuseIdentifier: CharacterCollectionViewCell.identifier)
        collectionView.backgroundColor = UIColor(named: "Black")
        collectionView.alwaysBounceVertical = true
        collectionView.showsVerticalScrollIndicator = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.isHidden = true
        return collectionView
    }()
    
    private lazy var spinner: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(style: .large)
        spinner.color = .green
        spinner.startAnimating()
        spinner.translatesAutoresizingMaskIntoConstraints = false
        return spinner
    }()
    
    private lazy var headerLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Gilroy-Bold", size: 28)
        label.text = "Characters"
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: Lifecycle
    
    init(charactersViewModel: CharactersViewModel) {
        self.charactersViewModel = charactersViewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadData()
        setupViewModelBindings()
    }
}

private extension CharactersViewController {
    
    func setupUI() {
        view.backgroundColor = UIColor(named: "Black")
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: headerLabel)
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        view.addSubview(spinner)
        view.addSubview(collectionView)

        NSLayoutConstraint.activate([
            spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
        ])
    }
    
    func loadData() {
        charactersViewModel.loadCharacters()
    }
    
    func setupViewModelBindings() {
        
        charactersViewModel.onCharactersUpdated = { [weak self] in
            self?.collectionView.reloadData()
        }
        
        charactersViewModel.onErrorMessage = { [weak self] error in
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
            alert.title = error.localizedDescription
            alert.message = error.localizedDescription
            
            self?.present(alert, animated: true)
        }
        
        charactersViewModel.onLoadingStateUpdated = { [weak self] in
            self?.spinner.stopAnimating()
            self?.spinner.removeFromSuperview()
            self?.collectionView.isHidden = false
        }
    }
    
    @objc func backButtonTapped() {
        navigationController?.popViewController(animated: true)
    }
}

extension CharactersViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return charactersViewModel.characters.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let backButton = UIBarButtonItem(image: UIImage(named: "backArrow"),
                                         style: .plain,
                                         target: self,
                                         action: #selector(backButtonTapped))
        backButton.tintColor = .white
        navigationItem.backBarButtonItem = backButton
        let characterId = charactersViewModel.characters[indexPath.row].id ?? 0
        let services = charactersViewModel.getServices()
        let characterViewModel = CharacterViewModel(characterId: characterId,
                                                    characterService: services.0,
                                                    episodeService: services.1,
                                                    locationService: services.2)
        let characterView = CharacterView(characterViewModel: characterViewModel)
        let controller = UIHostingController(rootView: characterView)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CharacterCollectionViewCell.identifier, for: indexPath) as? CharacterCollectionViewCell else {
            fatalError("Failed to dequeue CharacterCell in CharacterViewController")
        }
        let character = charactersViewModel.characters[indexPath.row]
        cell.configure(with: character)
        return cell
    }
}

extension CharactersViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.view.frame.width / 2) - 31.5
        let height = (self.view.frame.height / 4) - 4
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 31, left: 20, bottom: 0, right: 27)
    }
}
