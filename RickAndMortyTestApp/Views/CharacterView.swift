
import SwiftUI

struct CharacterView: View {

    @ObservedObject var characterViewModel: CharacterViewModel
    
    var body: some View {
        // View
        ScrollView(.vertical, showsIndicators: false) {
            if characterViewModel.character == nil {
                Loader()
            } else {
                VStack(spacing: 24) {
                    // Image
                    AsyncImage(url: URL(string: characterViewModel.character?.image ?? "")) { image in
                        image
                            .resizable()
                            .scaledToFill()
                        
                    } placeholder: {
                        ProgressView()
                    }
                    .frame(width: 148, height: 148, alignment: .center)
                    .cornerRadius(16)
                    .padding(.top, 16)
                    
                    // Name and status block
                    VStack(spacing: 8) {
                        Text(verbatim: characterViewModel.character?.name ?? "")
                            .font(.custom("Gilroy-Bold", size: 22))
                            .foregroundColor(.white)
                        
                        Text(verbatim: characterViewModel.character?.status ?? "")
                            .font(.custom("Gilroy-Medium", size: 16))
                            .foregroundColor(Color("Green"))
                    }
                    
                    // Info block
                    VStack(spacing: 16) {
                        BlockHeaderTextView(text: "Info")
                        
                        VStack(spacing: 16) {
                            InfoLine(header: "Species:", field: characterViewModel.character?.species ?? "")
                            InfoLine(header: "Type:", field: characterViewModel.character?.type ?? "")
                            InfoLine(header: "Gender:", field: characterViewModel.character?.gender ?? "")
                        }
                        .frame(maxWidth: .infinity)
                        .padding(16)
                        .background(Color("CardBackground"))
                        .font(.custom("Gilroy-Regular", size: 16))
                        .cornerRadius(16)
                    }
                    
                    // Origin block
                    VStack(spacing: 16) {
                        BlockHeaderTextView(text: "Origin")
                        
                        if characterViewModel.location == nil {
                            Loader()
                        } else {
                            HStack(spacing: 16) {
                                Image("PlanetIcon")
                                    .resizable()
                                    .scaledToFill()
                                    .frame(width: 24, height: 24, alignment: .center)
                                    .padding(20)
                                    .background(Color("PlanetIconBackground"))
                                    .cornerRadius(10)
                                
                                VStack(spacing: 8) {
                                    
                                    Text(verbatim: characterViewModel.location?.name ?? "unknown")
                                        .foregroundColor(.white)
                                        .font(.custom("Gilroy-SemiBold", size: 17))
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                    
                                    Text(verbatim: characterViewModel.location?.type ?? "unknown")
                                        .foregroundColor(Color("Green"))
                                        .font(.custom("Gilroy-Medium", size: 13))
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                }
                    
                            }
                            .padding(8)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .background(Color("CardBackground"))
                            .cornerRadius(10)
                        }
                    }.onAppear(perform: {
                        characterViewModel.loadOrigin()
                    })
                    
                    VStack(spacing: 16) {
                        
                        BlockHeaderTextView(text: "Episodes")
                        
                        if(characterViewModel.episodes.isEmpty) {
                            Loader()
                        } else {
                            VStack(spacing: 16) {
                                ForEach(characterViewModel.episodes, id:\.self) { episode in
                                    EpisodeCard(episode: episode)
                                }
                            }
                        }
                    }
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .onAppear(perform: {
                        characterViewModel.loadEpisodes()
                    })
                }
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding(.horizontal, 24)
        .background(Color("Black").ignoresSafeArea())
        .onAppear(perform: {
            characterViewModel.loadCharacterInfo()
        })
    }
}

struct CharacterView_Previews: PreviewProvider {
    static var previews: some View {
        let networkService = NetworkService(baseURL: "https://rickandmortyapi.com/api")
        let characterService = CharacterService(networkService: networkService)
        let episodeService = EpisodeService(networkService: networkService)
        let locationService = LocationService(networkService: networkService)
        
        CharacterView(characterViewModel: CharacterViewModel(
            characterId: 1,
            characterService: characterService,
            episodeService: episodeService,
            locationService: locationService))
    }
}

struct BlockHeaderTextView: View {
    
    let text: String
    
    var body: some View {
        HStack {
            Text(verbatim: text)
                .font(.custom("Gilroy-Bold", size: 17))
                .foregroundColor(.white)
        }
        .frame(maxWidth: .infinity, alignment: .leading)
    }
}

struct InfoLine: View {
    
    let header: String
    let field: String
    
    var body: some View {
        HStack {
            
            Text(verbatim: header)
                .foregroundColor(Color("Gray"))
            
            Spacer()
            
            Text(verbatim: field == "" ? "None" : field)
                .foregroundColor(.white)
            
        }
    }
}

struct EpisodeCard: View {
    
    let episode: EpisodeModel
    
    var body: some View {
        let rawEpisodeInfo = episode.episode ?? ""
        let season = String(Int(rawEpisodeInfo.components(separatedBy: "S").last?.components(separatedBy: "E").first ?? "") ?? 0)
        let episodeNumber = String(Int(rawEpisodeInfo.components(separatedBy: "E").last ?? "") ?? 0)
        
        let episodeInfo = "Episode: \(episodeNumber), Season: \(season)"
        
        VStack(spacing: 16) {
            Text(verbatim: episode.name ?? "")
                .font(.custom("Gilroy-Bold", size: 17))
                .foregroundColor(.white)
                .frame(maxWidth: .infinity, alignment: .leading)
            
            HStack {
                Text(verbatim: episodeInfo)
                    .font(.custom("Gilroy-Regular", size: 13))
                    .foregroundColor(Color("Green"))
                
                Spacer()
                
                Text(verbatim: episode.airDate ?? "Nill")
                    .font(.custom("Gilroy-Regular", size: 12))
                    .foregroundColor(Color("Gray"))
            }
        }
        .padding(16)
        .background(Color("CardBackground"))
        .cornerRadius(16)
    }
}

struct Loader: View {
    var body: some View {
        ProgressView("Loading")
            .tint(Color("Green"))
            .foregroundColor(Color("Green"))
            .frame(maxHeight: .infinity, alignment: .center)
    }
}
