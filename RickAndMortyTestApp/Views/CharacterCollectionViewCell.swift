
import UIKit
import Kingfisher

final class CharacterCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "CharacterCollectionViewCell"
    private let inset: CGFloat = 8
    
    private lazy var characterImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 10
        imageView.tintColor = .label
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var characterNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Gilroy-SemiBold", size: 17)
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with character: CharacterModelShort) {
        characterImageView.kf.indicatorType = .activity
        characterImageView.kf.setImage(
            with: URL(string: character.image ?? ""),
            options: [
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ]
        )
        
        let attributedText = NSAttributedString(
                    string: character.name ?? "",
                    attributes: [.kern: -0.41]
                )

        characterNameLabel.attributedText = attributedText
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        characterImageView.image = nil
        characterNameLabel.text = nil
    }
}

extension CharacterCollectionViewCell {
    
    func setupUI() {
        
        backgroundColor = UIColor(named: "CardBackground")
        layer.cornerRadius = 16
        addSubview(characterImageView)
        addSubview(characterNameLabel)
        
        NSLayoutConstraint.activate([
            characterImageView.topAnchor.constraint(equalTo: topAnchor, constant: inset),
            characterImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: inset),
            characterImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -inset),
            characterImageView.bottomAnchor.constraint(equalTo: characterNameLabel.topAnchor, constant: -inset * 2),
            
            characterNameLabel.topAnchor.constraint(equalTo: characterImageView.bottomAnchor, constant: inset * 2),
            characterNameLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            characterNameLabel.widthAnchor.constraint(lessThanOrEqualTo: widthAnchor, multiplier: 0.8),
            characterNameLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -inset * 2)
        ])
    }
}
