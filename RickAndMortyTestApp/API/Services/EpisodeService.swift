
import Foundation

final class EpisodeService: EpisodeServiceProtocol {
    
    let network: NetworkServiceProtocol
    
    init(networkService: NetworkServiceProtocol) {
        network = networkService
    }
    
    func getEpisodesInfo(episodeId: Int, completion: @escaping (Result<EpisodeModel, NetworkServiceError>) -> ()) {
        network.baseRequest(parameters: nil,
                            endpointPath: "/episode/\(episodeId)",
                            completionHandler: { (result: Result<EpisodeModel, NetworkServiceError>) in
            let mappedResult: Result<EpisodeModel, NetworkServiceError> = result.map({ return $0 })
            completion(mappedResult)
        })
    }
}

protocol EpisodeServiceProtocol {
    
    func getEpisodesInfo(episodeId: Int, completion: @escaping (Result<EpisodeModel, NetworkServiceError>) -> ())
}
