
import Foundation

final class CharacterService: CharacterServiceProtocol {
    
    let network: NetworkServiceProtocol
    
    init(networkService: NetworkServiceProtocol) {
        network = networkService
    }
    
    func getCharacters(page: Int, completion: @escaping (Result<[CharacterModelShort], NetworkServiceError>) -> ()) {
        
        let queryParameters = [URLQueryItem(name: "page", value: String(page))]
        
        network.baseRequest(parameters: queryParameters,endpointPath: "/character", completionHandler: { (result: Result<ResponseDataModel, NetworkServiceError>) in
            
            let finalResult: Result<[CharacterModelShort], NetworkServiceError> = result.map({ return $0.results ?? [] })
            completion(finalResult)
        })
    }
    
    func getCharacterInfo(characterId: Int, completion: @escaping (Result<CharacterModel, NetworkServiceError>) -> ()) {
        network.baseRequest(parameters: nil,endpointPath: "/character/\(characterId)", completionHandler: { (result: Result<CharacterModel, NetworkServiceError>) in
            
            let finalResult: Result<CharacterModel, NetworkServiceError> = result.map({ return $0 })
            completion(finalResult)
        })
    }
}

protocol CharacterServiceProtocol {
    
    func getCharacters(page: Int, completion: @escaping (Result<[CharacterModelShort], NetworkServiceError>) -> ())
    
    func getCharacterInfo(characterId: Int, completion: @escaping (Result<CharacterModel, NetworkServiceError>) -> ())
}
