
import Foundation

final class LocationService: LocationServiceProtocol {
    
    let network: NetworkServiceProtocol
    
    init(networkService: NetworkServiceProtocol) {
        network = networkService
    }
    
    func getLocation(locationId: Int, completion: @escaping (Result<LocationModel, NetworkServiceError>) -> ()) {
        network.baseRequest(parameters: nil,endpointPath: "/location/\(locationId)", completionHandler: { (result: Result<LocationModel, NetworkServiceError>) in
            
            let finalResult: Result<LocationModel, NetworkServiceError> = result.map({ return $0 })
            completion(finalResult)
        })
    }
}

protocol LocationServiceProtocol {
    
    func getLocation(locationId: Int, completion: @escaping (Result<LocationModel, NetworkServiceError>) -> ())
}
