import Foundation

final class NetworkService: NetworkServiceProtocol {
    
    let baseURL: String
    
    init(baseURL: String){
        self.baseURL = baseURL
    }
    
    func baseRequest<ResponseType: Decodable>(parameters: [URLQueryItem]?,endpointPath: String, completionHandler: @escaping (Result<ResponseType, NetworkServiceError>) -> () ) {
        
        guard let url = URL(string: baseURL)?.appendingPathComponent(endpointPath) else {
            completionHandler(.failure(NetworkServiceError.incorrectURL))
            return
        }
        
        var request = URLRequest(url: url)
        
        if let params = parameters {
            var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true)
            urlComponents?.queryItems = params
            request = URLRequest(url: urlComponents!.url!)
        }
    
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { [weak self] (data, response, error) in
            guard let self = self else { return }
            let result: Result<ResponseType, NetworkServiceError> = taskResult(data: data, response: response, error: error)
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }.resume()
        
    }
}

protocol NetworkServiceProtocol {
    
    func baseRequest<ResponseType: Decodable>(parameters: [URLQueryItem]?,
                                              endpointPath: String,
                                              completionHandler: @escaping (Result<ResponseType, NetworkServiceError>) -> ())
}
